package co.simplon.promo18.gstockspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GstockSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(GstockSpringApplication.class, args);
	}

}

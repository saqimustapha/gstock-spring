package co.simplon.promo18.gstockspring.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.gstockspring.entities.Category;
import co.simplon.promo18.gstockspring.entities.Product;

@Repository
public class ProductRepository {

  @Autowired
  private DataSource dataSource;


  public List<Product> findAll() {
    List<Product> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM product INNER JOIN category AS c ON id_category = c.id");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Product product = new Product(
          rs.getInt("id"),
          rs.getString("name"),
          rs.getDouble("price"),
          rs.getInt("quantity")
        );
        Category category = new Category(
          rs.getInt("id_category"),
          rs.getString("designation")
        );
        product.setCategory(category);
        list.add(product);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return list;
  }

  public Product findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM product INNER JOIN category AS c ON id_category = c.id WHERE product.id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Product product = new Product(
          rs.getInt("id"),
          rs.getString("name"), 
          rs.getDouble("price"),
          rs.getInt("quantity")
          );

        Category category = new Category(
          rs.getInt("id_category"),
          rs.getString("designation")
          );
          product.setCategory(category);


        return product;
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return null;
  }

  public void save(Product product) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO product (name, price, quantity, id_category) VALUES (?,?,?,?)",
              Statement.RETURN_GENERATED_KEYS);

      stmt.setString(1, product.getName());
      stmt.setDouble(2, product.getPrice());
      stmt.setInt(3, product.getQuantity());
      stmt.setInt(4, product.getCategory().getId());

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        product.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

  }

  public boolean delete(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM product WHERE id=?");
      stmt.setInt(1, id);

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }
  }

  public boolean update(Product product) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection
          .prepareStatement("UPDATE product SET name = ?, price = ?, quantity = ? WHERE id = ?");

      stmt.setString(1, product.getName());
      stmt.setDouble(2, product.getPrice());
      stmt.setInt(3, product.getQuantity());
      stmt.setInt(4, product.getId());

      return (stmt.executeUpdate() == 1);

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

  }

  public List<Product> searchByName(String name) {
    List<Product> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement(

          "SELECT * FROM product WHERE name LIKE ?");
      stmt.setString(1, "%" + name + "%");
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Product product = new Product(
          rs.getInt("id"),
          rs.getString("name"),
          rs.getDouble("price"),
          rs.getInt("quantity"));

        list.add(product);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return list;
  }

  public List<Product> findByIdCategory(int idCategory) {
    List<Product> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
        PreparedStatement stmt = connection.prepareStatement("SELECT * FROM product WHERE id_category=?");

        stmt.setInt(1, idCategory);

        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
          Product product = new Product(
            rs.getInt("id"),
            rs.getString("name"),
            rs.getDouble("price"),
            rs.getInt("quantity"));
  
          list.add(product);
        }
      } catch (SQLException e) {
  
        e.printStackTrace();
        throw new RuntimeException("Database access error");
      }
  
      return list;
  }
}


package co.simplon.promo18.gstockspring.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.gstockspring.entities.Ordering;
import co.simplon.promo18.gstockspring.repository.OrderingRepository;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/ordering")
public class OrderingController {

  @Autowired
  private OrderingRepository or;

  @GetMapping
  public List<Ordering> all() { 
      return or.findAll();
  }
 
  @GetMapping("/{id}")
  public Ordering one(@PathVariable int id) {
    Ordering ordering = or.findById(id);
    if (ordering == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return ordering;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Ordering addOrdering(@RequestBody Ordering ordering) {
    ordering.setId(null);

    or.save(ordering);

    return ordering;
  }

  
}

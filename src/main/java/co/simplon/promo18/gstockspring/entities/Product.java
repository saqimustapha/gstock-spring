package co.simplon.promo18.gstockspring.entities;

public class Product {
  
  private Integer id;
  private String name;
  private Double price;
  private int quantity;
  private Category category;
  private Ordering ordering;
  
  public Category getCategory() {
    return category;
  }
  public void setCategory(Category category) {
    this.category = category;
  }
  public Product() {}
  public Product(String name, Double price, int quantity) {
    this.name = name;
    this.price = price;
    this.quantity = quantity;
  }
  public Product(Integer id, String name, Double price, int quantity) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.quantity = quantity;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public Double getPrice() {
    return price;
  }
  public void setPrice(Double price) {
    this.price = price;
  }
  public int getQuantity() {
    return quantity;
  }
  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }
  public Ordering getOrdering() {
    return ordering;
  }
  public void setOrdering(Ordering ordering) {
    this.ordering = ordering;
  }

}

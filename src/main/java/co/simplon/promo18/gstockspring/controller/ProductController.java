package co.simplon.promo18.gstockspring.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.gstockspring.entities.Product;

import co.simplon.promo18.gstockspring.repository.ProductRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/product")
public class ProductController {

  @Autowired
  private ProductRepository repo;
 

  @GetMapping
  public List<Product> all(@RequestParam Optional<String> name) {
    if (!name.isPresent()) {
      return repo.findAll();
    } else {
      return repo.searchByName(name.get());
    }
  }

  @GetMapping("/{id}")
  public Product one(@PathVariable int id) {
    Product product = repo.findById(id);
    if (product == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return product;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Product addProduct(@RequestBody Product product) {
    product.setId(null);

    repo.save(product);

    return product;
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id) {
    if (!repo.delete(id)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/{id}")
  public Product update(@RequestBody Product product, @PathVariable int id) {
    if (id != product.getId()) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }
    if (!repo.update(product)) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return repo.findById(product.getId());
  }

  @PatchMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public Product patch(@RequestBody Product product, @PathVariable int id) {
    Product basePr = repo.findById(id);
    if (basePr == null)
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    if (product.getName() != null)
      basePr.setName(product.getName());
    if (product.getPrice() != 0.0)
      basePr.setPrice(product.getPrice());
    if (product.getQuantity() != 0)
      basePr.setQuantity(product.getQuantity());
    if (!repo.update(basePr))
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    return basePr;
  }
  
  @GetMapping("/category/{id}")
  public List<Product> getProductsByCategory(@PathVariable int id){
    List<Product> products=repo.findByIdCategory(id);
    return products;
  }

}

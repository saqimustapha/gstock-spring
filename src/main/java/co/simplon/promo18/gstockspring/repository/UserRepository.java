package co.simplon.promo18.gstockspring.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.gstockspring.entities.User;

@Repository
public class UserRepository {
    @Autowired
    private DataSource dataSource;

    public List<User> findAll() {
      List<User> list = new ArrayList<>();
      try (Connection connection = dataSource.getConnection()) {
          PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user");

          ResultSet rs = stmt.executeQuery();

          while (rs.next()) {
              User user = new User(
                      rs.getInt("id"),
                      rs.getString("email"),
                      rs.getString("password"),
                      rs.getString("role"));

              list.add(user);
          }
      } catch (SQLException e) {
          
          e.printStackTrace();
          throw new RuntimeException("Database access error");
      }

      return list;
  }





    public User findByEmail(String email) {
        
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM user WHERE email=?");

            stmt.setString(1, email);

            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                User user = new User(
                        rs.getInt("id"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("role"));
                return user;        
  
            }
        } catch (SQLException e) {
            
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return null;
    }

    public void save(User user) {
      try (Connection connection = dataSource.getConnection()) {
          PreparedStatement stmt = connection.prepareStatement("INSERT INTO user (email, password, role) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);

          stmt.setString(1, user.getEmail());
          stmt.setString(2, user.getPassword());
          stmt.setString(3, user.getRole());

          stmt.executeUpdate();

          ResultSet rs = stmt.getGeneratedKeys();
          if(rs.next()) {
              user.setId(rs.getInt(1));
          }
       
      } catch (SQLException e) {
          
          e.printStackTrace();
          throw new RuntimeException("Database access error");
      }

  }



}
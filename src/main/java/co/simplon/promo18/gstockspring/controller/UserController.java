package co.simplon.promo18.gstockspring.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.gstockspring.auth.ChangePasswordDto;
import co.simplon.promo18.gstockspring.entities.User;
import co.simplon.promo18.gstockspring.repository.UserRepository;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private PasswordEncoder encoder;

    @GetMapping
    public String test(@AuthenticationPrincipal User user) {
        if(user != null) {
            return "Hello "+user.getEmail();
        }
        return "not connected";
    }

    @GetMapping("/account")
    public User getAccount(@AuthenticationPrincipal User user) {
        return user;
    }

    @PostMapping
    public User register(@Valid @RequestBody User user) {
        if(userRepo.findByEmail(user.getEmail()) !=null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }
        user.setId(null);
        user.setRole("ROLE_USER"); 
       
        String hashed = encoder.encode(user.getPassword());
        
        user.setPassword(hashed);
        userRepo.save(user);
        

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));
        
        return user;
    }

    @PatchMapping("/password")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changePassword(@RequestBody ChangePasswordDto body, @AuthenticationPrincipal User user) {
        if(!encoder.matches(body.oldPassword, user.getPassword())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Old Password doesn't match");
        }

        String hashed = encoder.encode(body.newPassword);
        user.setPassword(hashed);
        userRepo.save(user);
    }
    

}


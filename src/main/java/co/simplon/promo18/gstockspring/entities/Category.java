package co.simplon.promo18.gstockspring.entities;

import java.util.ArrayList;
import java.util.List;

public class Category {

  private Integer id;
  private String designation;

  private List<Product> products = new ArrayList<>();
  
  public List<Product> getProducts() {
    return products;
  }
  public void setProducts(List<Product> products) {
    this.products = products;
  }
  public Category() {}
  public Category(String designation) {
    this.designation = designation;
  }
  public Category(Integer id, String designation) {
    this.id = id;
    this.designation = designation;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getDesignation() {
    return designation;
  }
  public void setDesignation(String designation) {
    this.designation = designation;
  }
  
}

package co.simplon.promo18.gstockspring.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import co.simplon.promo18.gstockspring.entities.User;
import co.simplon.promo18.gstockspring.repository.UserRepository;



@Service
public class AuthService implements UserDetailsService{

    @Autowired
    private UserRepository repo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

      User user = repo.findByEmail(email);
      if(user == null) {
          throw new UsernameNotFoundException("User not found");
      }
        return user;
      }
}   
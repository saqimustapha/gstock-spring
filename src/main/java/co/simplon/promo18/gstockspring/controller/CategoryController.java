package co.simplon.promo18.gstockspring.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.gstockspring.entities.Category;
import co.simplon.promo18.gstockspring.repository.CategoryRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/category")
public class CategoryController {

  @Autowired
  private CategoryRepository cr;

  @GetMapping
  public List<Category> all() {
    return cr.findAll();
  }

  @GetMapping("/{id}")
  public Category one(@PathVariable int id) {
    Category category = cr.findById(id);
    if (category == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return category;
  }


}

-- Active: 1648455619919@@127.0.0.1@3306@p18_gstock

DROP TABLE IF EXISTS category;

DROP TABLE IF EXISTS product;

DROP TABLE IF EXISTS ordering;

DROP TABLE IF EXISTS user;

CREATE TABLE category(
    id INT PRIMARY KEY AUTO_INCREMENT,
    designation VARCHAR(128)
    
);

CREATE TABLE product(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128),
    quantity INT NOT NULL,
    price DOUBLE NOT NULL,
    id_category INT,
    FOREIGN KEY (id_category) REFERENCES category(id) ON DELETE CASCADE
);


CREATE TABLE ordering(
    id INT PRIMARY KEY AUTO_INCREMENT,
    quantity INT NOT NULL,
    date DATE NOT NULL,
    id_product INT, 
    FOREIGN KEY (id_product) REFERENCES product(id) ON DELETE CASCADE
);

CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(128) ,
    password VARCHAR(64),
    role VARCHAR(128)  
);

INSERT INTO category (designation) VALUES ("Électroménager"),
("Informatique"),
("Téléphonie"),
("Image & Son");

INSERT INTO product (name, quantity, price, id_category) VALUES (" PC portable", 3, 550.99, 2),
("Lave-linge", 5, 450.50, 1),
("Télévision", 7, 750.50, 4),
("Ecouteurs", 11, 19.90, 4),
("Home Cinéma", 7, 750.50, 4),
("Smartphone", 9, 350.80, 3),
("Iphone", 8, 400.90, 3),
("Téléphone fixe", 7, 50.50, 3),
("Réfrigirateur", 4, 675.90, 1),
("PC de bureau", 4, 675.90, 2),
("Tablette", 10, 275.90, 2),
("Lave-vaisselle", 9, 420.99, 1);
 
INSERT INTO ordering (quantity, date, id_product) VALUES (5, "2022-03-04", 1),
(9, "2022-04-28", 2),
(5, "2022-03-04", 1);

USE p18_gstock;

SELECT * FROM product
INNER JOIN category AS c 
ON id_category = c.id;

SELECT * FROM ordering
LEFT JOIN product AS p
ON id_product = p.id;





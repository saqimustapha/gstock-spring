package co.simplon.promo18.gstockspring.entities;

import java.util.Collection;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import com.fasterxml.jackson.annotation.JsonProperty;


public class User implements UserDetails {

    
    private Integer id;
    @Email
    @NotBlank
    private String email;
    @NotBlank
    @Size(min=4, max=64)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) //On ignore le password en Json en lecture pour que le mot de passe se balade pas trop
    private String password;
    private String role;

    public User(@Email @NotBlank String email, @NotBlank @Size(min = 4, max = 64) String password,
        String role) {
      this.email = email;
      this.password = password;
      this.role = role;
    }
    public User() {}
    public User(Integer id, @Email @NotBlank String email,
        @NotBlank @Size(min = 4, max = 64) String password, String role) {
      this.id = id;
      this.email = email;
      this.password = password;
      this.role = role;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(
            new SimpleGrantedAuthority(role)
        );
    }
    @Override
    public String getUsername() {
        return email;
    }
    
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
    
}

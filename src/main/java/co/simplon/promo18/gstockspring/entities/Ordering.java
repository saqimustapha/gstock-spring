package co.simplon.promo18.gstockspring.entities;

import java.time.LocalDate;

public class Ordering {

  private Integer id;
  private int quantity;
  private LocalDate date;
  private Product product;
  
  public Ordering() {}
  public Ordering(int quantity, LocalDate date) {
    this.quantity = quantity;
    this.date = date;
  }
  public Ordering(Integer id, int quantity, LocalDate date) {
    this.id = id;
    this.quantity = quantity;
    this.date = date;
  }
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public int getQuantity() {
    return quantity;
  }
  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }
  public LocalDate getDate() {
    return date;
  }
  public void setDate(LocalDate date) {
    this.date = date;
  }
  public Product getProduct() {
    return product;
  }
  public void setProduct(Product product) {
    this.product = product;
  }
 
}

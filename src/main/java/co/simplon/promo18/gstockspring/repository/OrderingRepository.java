package co.simplon.promo18.gstockspring.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.gstockspring.entities.Ordering;
import co.simplon.promo18.gstockspring.entities.Product;

@Repository
public class OrderingRepository {

  @Autowired
  private DataSource dataSource;

  public List<Ordering> findAll() {
    List<Ordering> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ordering LEFT JOIN product AS p ON id_product = p.id");
      
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Ordering ordering = new Ordering(
          rs.getInt("id"),
          rs.getInt("quantity"),
          rs.getDate("date").toLocalDate()
          );  
        Product product = new Product(
            rs.getInt("id_product"), 
            rs.getString("name"),
            rs.getDouble("price"),
            rs.getInt("quantity")
          );
          ordering.setProduct(product);

        list.add(ordering);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return list;
  }

  public Ordering findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM ordering LEFT JOIN product AS p ON id_product = p.id WHERE ordering.id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Ordering ordering = new Ordering(
         rs.getInt("id"),
         rs.getInt("quantity"),
         rs.getDate("date").toLocalDate()
         );
        
        Product product = new Product(
          rs.getInt("id_product"), 
          rs.getString("name"),
          rs.getDouble("price"),
          rs.getInt("quantity")
          );
          ordering.setProduct(product); 
          
        return ordering;
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return null;
  }

  public void save(Ordering ordering) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt =
          connection.prepareStatement("INSERT INTO ordering (quantity, date, id_product) VALUES (?,?,?)",
              Statement.RETURN_GENERATED_KEYS);

      stmt.setInt(1, ordering.getQuantity());
      stmt.setDate(2, Date.valueOf(ordering.getDate()));
      stmt.setInt(3, ordering.getProduct().getId());
      

      stmt.executeUpdate();

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        ordering.setId(rs.getInt(1));
      }

    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

  }

  
}

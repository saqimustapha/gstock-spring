package co.simplon.promo18.gstockspring.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.gstockspring.entities.Category;

@Repository
public class CategoryRepository {

  @Autowired
  private DataSource dataSource;


  public List<Category> findAll() {
    List<Category> list = new ArrayList<>();
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category");

      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Category category = new Category(
          rs.getInt("id"),
          rs.getString("designation"));

        list.add(category);
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return list;
  }

  public Category findById(int id) {
    try (Connection connection = dataSource.getConnection()) {
      PreparedStatement stmt = connection.prepareStatement("SELECT * FROM category WHERE id=?");

      stmt.setInt(1, id);

      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Category category = new Category(
          rs.getInt("id"),
          rs.getString("designation")); 
          
        return category;
      }
    } catch (SQLException e) {

      e.printStackTrace();
      throw new RuntimeException("Database access error");
    }

    return null;
  } 
  
}

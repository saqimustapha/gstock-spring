package co.simplon.promo18.gstockspring.auth;

public enum UserRoles {
    ROLE_USER,
    ROLE_ADMIN
}
